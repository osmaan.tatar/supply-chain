import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzThSelectionComponent } from 'ng-zorro-antd/table';
import { PhoneService } from 'src/app/services/phone-service';

interface Phone {
 
  Key: string;
  Record: {
  brand: string;
  color: string;
  docType: string;
  imei: string;
  model: string;
  owner: null}

}

interface NewOwner {
  phoneId: string;
  newOwner: string;
}

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  isVisible = false;
  isVisibleEdit= false;
  listOfData: Phone[] = [];
  validateForm: FormGroup;
  editForm: FormGroup;
  errors: any;
  currentUser: any;
  phone: any;
  currentRole;

  constructor(private phoneService: PhoneService,
              private fb: FormBuilder) {
                this.validateForm = this.fb.group({
                  phoneNumber:['', [Validators.required]],
                  brand: ['', [Validators.required]],
                  color: ['',  Validators.required],
                  owner: [null],
                  imei: ['',[Validators.required] ],
                  model: ['', [Validators.required]],
                });

                this.editForm = this.fb.group({
                  phoneNumber:['', [Validators.required]],
                  brand: ['', [Validators.required]],
                  color: ['',  Validators.required],
                  owner: [null],
                  imei: ['',[Validators.required] ],
                  model: ['', [Validators.required]],
                })
               this.currentUser = localStorage.getItem('username');
               this.currentRole = localStorage.getItem('role');
              }


  ngOnInit() {
    this.getPhones();
  }
  
  getPhones() {
    this.phoneService.getPhones().subscribe( res => {
      this.listOfData = res;
    })
  }

  addRow(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  handleCancelEdit(): void {
    console.log('Button cancel clicked!');
    this.isVisibleEdit = false;
  }
  

  submitForm(): void {
    const phone = "PHONE" + this.validateForm.controls.phoneNumber.value;
    this.validateForm.controls.phoneNumber.setValue(phone);
    for (const key in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(key)) {
        this.validateForm.controls[key].markAsDirty();
        this.validateForm.controls[key].updateValueAndValidity();
      }
    }
    this.phoneService.addPhone(this.validateForm.value).subscribe( res => {
      this.getPhones();
    },
      error => {
          this.errors = error
      }
    ); 
    if (!this.errors) {
      this.isVisible =false;
    }
  }

  approve(id: any): void {
    this.getPhone(id);
  }

  getPhone(id: any) {
    this.phoneService.getPhone(id).subscribe(res => {
      this.phone = res;
      this.editFormData(res, id);
    })
  }

  editFormData(res: any, id: any) {
    this.editForm.controls.phoneNumber.setValue(id);
    this.editForm.controls.brand.setValue(res.brand);
    this.editForm.controls.color.setValue(res.color);
    this.editForm.controls.owner.setValue(res.owner);
    this.editForm.controls.imei.setValue(res.imei);
    this.editForm.controls.model.setValue(res.model);
    this.isVisibleEdit = true;

  }

  approveOwner(){
    const owner = {phoneId: this.editForm.controls.phoneNumber.value, newOwner: this.currentUser} as NewOwner;
    this.phoneService.changeOwner(owner).subscribe(
      error => {
          this.errors = error
      }
    ); 
    if (!this.errors) {
      this.phoneService.getPhone(owner.phoneId).subscribe(res => {
        this.phone = res;
        this.editFormData(res, owner.phoneId);
      })
    }
  }
}




