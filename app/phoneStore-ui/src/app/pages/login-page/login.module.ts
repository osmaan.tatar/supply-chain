import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { DemoNgZorroAntdModule } from 'src/ng-zorro-antd.module';
import { LoginPageComponent } from './login-page.component';
import { LoginRoutingModule } from './login-routing.module';



@NgModule({
  imports: [LoginRoutingModule, DemoNgZorroAntdModule, CommonModule, FormsModule, ReactiveFormsModule],
  declarations: [LoginPageComponent],
  exports: [LoginPageComponent],
  providers: [
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService
]
})
export class LoginModule { }
