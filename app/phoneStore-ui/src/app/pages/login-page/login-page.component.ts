import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoginDto } from 'src/app/dtos/loginDto';
import { AuthService } from 'src/app/services/auth-service';


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  validateForm!: FormGroup;
  

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(i)) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    const loginData = {username: this.validateForm.controls.username.value, password: this.validateForm.controls.password.value} as LoginDto;
    this.authService.login(loginData).subscribe(res => {
      const decodeToken = this.jwtHelperService.decodeToken(localStorage.getItem('token') || '{}');
      localStorage.setItem('username', decodeToken.username);
      localStorage.setItem('role', decodeToken.role);
      this.router.navigate(['phone'])
             
    });
  }

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private jwtHelperService: JwtHelperService,
              public router: Router) {
               
              }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }
}
