import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/auth' },
  { path: 'phone', loadChildren: () => import('./pages/welcome/welcome.module').then(m => m.WelcomeModule)},
   { path: 'auth', loadChildren: () => import('./pages/login-page/login.module').then(m => m.LoginModule)} 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
