import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from 'src/environments/environment';

interface Config {
  pageable?: true;

  [key: string]: any;
}

@Injectable({
  providedIn: 'root'
})
export abstract class PhoneService {

    baseApiUrl: string;

    constructor(public http: HttpClient,
               ) {
      this.baseApiUrl = environment.baseUrl;
    }

    public getPhones(): Observable<any>{
        return this.http.get('api/getPhones')
    }

    public addPhone(data: any): Observable<any>{
      return this.http.post('api/addPhone', data);
    }

    public getPhone(id: string): Observable<any>{
      return this.http.get('api/phone/get?id='+ id)
    }

    public changeOwner(data: any): Observable<any>{
      return this.http.post('api/changeOwner', data);
    }
}
