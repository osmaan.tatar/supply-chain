import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

interface Config {
  pageable?: true;

  [key: string]: any;
}

@Injectable({
  providedIn: 'root'
})
export abstract class AuthService {

    baseApiUrl: string;

    constructor(public http: HttpClient,
                private jwtHelper: JwtHelperService
               ) {
      this.baseApiUrl = environment.baseUrl;
    }

    public login(data: any): Observable<any>{
      return this.http.post('api/auth/login', data).pipe(map((response: any) => {
        const tokenDto = response.token;
        if (tokenDto) {
          localStorage.setItem('token', tokenDto);
        }
        return response;
      }));
    }

    isLoggedIn() {
        let token: string | null;
        token = localStorage.getItem('token');
        if (token != null) {
          return !this.jwtHelper.isTokenExpired(token);
        }
        return false;
      }

}
