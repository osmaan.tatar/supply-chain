
var http = require('http');
var mysql = require('mysql');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
const cors          = require('cors');
const jwt           = require('jsonwebtoken');
var expressJWT      = require('express-jwt');
var app = express();
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use(cors());
app.options('*', cors());

var path = require('path');
let secret = 'empty'

var phoneStore = require('../../javascript/phone');

var connection = mysql.createConnection({
	host     : 'localhost',
    port     : '3306',
	user     : 'root',
	password : 'asdqwe123',
	database : 'blockchainlogin'
});


app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));

app.use(expressJWT({ secret: secret, algorithms: ['HS256']})
    .unless(
        { path: [
            '/auth/login'
        ]}
    ));




var server = http.createServer(app).listen(3001, function() { console.log('express is running at port 3001')});
console.log('Started')
server.timeout = 240000;

app.post('/auth/login', function(request, response) {
    var data = {
        "username": request.body.username,
        "password": request.body.password
    }
	if (data.username && data.password) {
		connection.query('SELECT * FROM accounts WHERE username = ? AND password = ?', [data.username, data.password], function(error, results, fields) {
			if (results !==undefined) {
                var resData = {
                    "username": results[0].username,
                    "role": results[0].role
                }
                let token = jwt.sign(resData, secret, { expiresIn: '15m'})
                response.status(200).json({"token": token});
			} else {
				response.send('Incorrect Username and/or Password!');
                console.log(error);
			}
			response.end();
		});
	} else {
		response.send('Please enter Username and Password!');
		response.end();
	}
});

app.get('/getPhones', (req, res)=> {
     phoneStore.getAllPhones(req.user?.username).then(function(response) {
        var result = response.toString();
        res.send(result);
    });
})

app.post('/addPhone', (req,res) => {
    var user = req.user?.username;
    var phoneId = req.body.phoneNumber;
    var brand = req.body.brand;
    var model = req.body.model;
    var color = req.body.color;
    var imei = req.body.imei;
    var owner = req.body.owner;
    phoneStore.addPhone(phoneId, brand, model, color , imei , owner, user).then(function(response) {
        var result = response.toString();
        res.send(result);
    });
})

app.get('/phone/get', function(req, res) {
    var phoneId = req.query.id;
    var user = req.user?.username;
    phoneStore.getPhone(phoneId.toString().toUpperCase(), user).then(function(response) {
        var result = response.toString();
        res.send(result);
    });
});

app.post('/changeOwner', (req,res) => {
    var user = req.user.username;
    var phoneId = req.body.phoneId;
    var newOwner = req.body.newOwner;
    phoneStore.changeOwner(phoneId, newOwner, user);
})




