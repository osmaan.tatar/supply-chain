/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class Phone extends Contract {

    async initLedger(ctx) {
        const phones = [
            {
                brand:'Apple',
                model:'Iphone 12',
                color:'gray',
                imei:'aaaaaaaaaa!2xd',
                owner: null,
            },
            {
                brand:'Samsung',
                model:'a20',
                color:'blue',
                imei:'samsungime',
                owner: null,
            },
            {
                brand:'Xiamio',
                model:'redmi note',
                color:'black',
                imei:'redmiimei',
                owner : null,
            },
            {
                brand:'Oppo',
                model:'a5',
                color:'red',
                imei:'oppeimei',
                owner: null,
            }
        ];

        for (let i = 0; i < phones.length; i++) {
            phones[i].docType = 'phone';
            await ctx.stub.putState('PHONE' + i, Buffer.from(JSON.stringify(phones[i])));
            console.info('Added <--> ', phones[i]);
        }
    }

    async queryPhone(ctx, phoneNumber) {
        const phoneAsBytes = await ctx.stub.getState(phoneNumber); 
        if (!phoneAsBytes || phoneAsBytes.length === 0) {
            throw new Error(`${phoneNumber} does not exist`);
        }
        console.log(phoneAsBytes.toString());
        return phoneAsBytes.toString();
    }

    async createPhone(ctx, phoneNumber, brand, model, color, imei , owner) {
        const phone = {
            color,
            docType: 'phone',
            brand,
            model,
            imei,
            owner,
        };

        await ctx.stub.putState(phoneNumber, Buffer.from(JSON.stringify(phone)));
        console.info('CREATED PHONE');
    }

    async queryAllPhones(ctx) {
        const startKey = 'PHONE0';
        const endKey = 'PHONE999';

        const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString('utf8'));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString('utf8'));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString('utf8');
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    async changeOwner(ctx, phoneNumber, newOwner) {

        const phoneAsBytes = await ctx.stub.getState(phoneNumber); 
        if (!phoneAsBytes || phoneAsBytes.length === 0) {
            throw new Error(`${phoneNumber} does not exist`);
        }
        const phone = JSON.parse(phoneAsBytes.toString());
        phone.owner = newOwner;

        await ctx.stub.putState(phoneNumber, Buffer.from(JSON.stringify(phone)));
    }

    async deletePhone(ctx, phoneNumber) {
        const phoneAsBytes = await ctx.stub.getState(phoneNumber); 
        if (!phoneAsBytes || phoneAsBytes.length === 0) {
            throw new Error(`${phoneNumber} does not exist`);
        }
        console.log(phoneAsBytes.toString());
        return phoneAsBytes.toString();
    }

};

module.exports = Phone;
