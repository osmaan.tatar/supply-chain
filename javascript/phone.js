// 'use strict';

const { FileSystemWallet, Gateway } = require('fabric-network');
const fs = require('fs');
const path = require('path');

const ccpPath = path.resolve(__dirname, '..', 'connection.json');
const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
const ccp = JSON.parse(ccpJSON);

async function getPhone(phoneId, user) {
    try {
        console.log(path);
        const walletPath = path.join(process.cwd(),'../../javascript', 'wallet');
        const wallet = new FileSystemWallet(walletPath);
        const userExists = await wallet.exists(user);
        if(!userExists){
            console.log('user does not exist in the wallet')
        }

        const gateway = new Gateway();
        await gateway.connect(ccp,{wallet, identity: user,discovery: {enabled: false}});

        const network = await gateway.getNetwork('mychannel');
        const contract = network.getContract('phonestore');

        const response = await contract.evaluateTransaction('queryPhone', phoneId);
        console.log(`Phone is: ${response.toString()}`);

        return response;

    } catch (error) {
        console.error(`Failed : ${error}`);
        process.exit(1);
    }
}

async function getAllPhones(user) {
    try {
        const walletPath = path.join(process.cwd(),'../../javascript', 'wallet');
        const wallet = new FileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        const userExists = await wallet.exists(user);
        if(!userExists){
            console.log('user does not exist in the wallet')
        }

        const gateway = new Gateway();
        await gateway.connect(ccp,{wallet, identity: user,discovery: {enabled: false}});

        const network = await gateway.getNetwork('mychannel');
        const contract = network.getContract('phonestore');

        const response = await contract.evaluateTransaction('queryAllPhones');
        console.log(`Phone is: ${response.toString()}`);

        return response;

    } catch (error) {
        console.error(`Failed : ${error}`);
        process.exit(1);
    }
}


async function addPhone(phoneId, color, brand, model, imei, owner, user) {
    try {
        const walletPath = path.join(process.cwd(),'../../javascript', 'wallet');
        const wallet = new FileSystemWallet(walletPath);

        const userExists = await wallet.exists(user);
        if(!userExists){
            console.log('user does not exist in the wallet')
        }

        const gateway = new Gateway();
        await gateway.connect(ccp,{wallet, identity: user,discovery: {enabled: false}});

        const network = await gateway.getNetwork('mychannel');
        const contract = network.getContract('phonestore');

        
        console.log( phoneId, color, brand, model, imei, owner)
        const res = await contract.submitTransaction('createPhone', phoneId, color, brand, model, imei, " ");
       
        await gateway.disconnect();

        return res;

    } catch (error) {
        console.error(`Error : ${error}`);
        process.exit(1);
    }
}

async function changeOwner(phoneId, newOwner, user) {
    try {
        const walletPath = path.join(process.cwd(),'../../javascript', 'wallet');
        const wallet = new FileSystemWallet(walletPath);

        const userExists = await wallet.exists(user);
        if(!userExists){
            console.log('user does not exist in the wallet')
        }

        const gateway = new Gateway();
        await gateway.connect(ccp,{wallet, identity: user,discovery: {enabled: false}});

        const network = await gateway.getNetwork('mychannel');
        const contract = network.getContract('phonestore');

        
        
        await contract.submitTransaction('changeOwner', phoneId, newOwner);
       
        await gateway.disconnect();

    } catch (error) {
        console.error(`Error : ${error}`);
        process.exit(1);
    }
}

function hello() {
	console.log('Hello');
}

exports.getPhone = getPhone;
exports.addPhone = addPhone;
exports.hello = hello;
exports.getAllPhones = getAllPhones;
exports.changeOwner = changeOwner;