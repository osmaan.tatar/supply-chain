'use strict';

const { FileSystemWallet, Gateway } = require('fabric-network');
const fs = require('fs');
const path = require('path');

const ccpPath = path.resolve(__dirname, '..', 'connection.json');
console.log("ccpPath",ccpPath)

const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
const ccp = JSON.parse(ccpJSON);

async function main() {
    
    try {
        const walletPath = path.join(process.cwd(), 'wallet');
        const wallet = new FileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        const userExists = await wallet.exists('user1');
        if(!userExists){
            console.log('user1 does not exist in the wallet')
        }

        const gateway = new Gateway();
        await gateway.connect(ccp,{wallet, identity: 'user1',discovery: {enabled: false}});

        const network = await gateway.getNetwork('mychannel');

        const contract = network.getContract('phonestore');

        const response = await contract.evaluateTransaction('queryAllPhones');
        console.log(`phones: ${response.toString()}`);

    } catch (error) {
        console.error(`error: ${error}`)
    }
}

main();